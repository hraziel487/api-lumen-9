<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\ApiResponser;
use App\Models\Videogame;

class VideogameController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        $videojuegos = Videogame::all();
        return $this->successResponse($videojuegos);
    }

    public function store(Request $request)
    {
     $rules = [
        'Nombre' => 'required|max:255',
        'Descripcion' => 'required|max:255',
        'Lanzamiento' => 'required|max:255',
        'Portada' => 'required|max:255',
        'Compania' => 'required|max:255',
        'Consolas' => 'required|max:255'
     ];
     
     $this->validate($request,$rules);
     $videojuegos = Videogame::create($request->all());
     if($request->hasfile('Portada')){
        $Portada = $request->file('Portada');
        $extension = $Portada->getClientOriginalExtension();
        $nombre = time() . '.' . $extension;
        $Portada->move('Portada/' , $nombre);
        
        $videojuegos->Portada=$nombre;
    }
    $videojuegos->save();
    return $this->successResponse($videojuegos, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $videojuego = Videogame::findOrFail($id);
        return $this->successResponse($videojuego);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'Nombre' => 'max:255',
            'Descripcion' => 'max:255',
            'Lanzamiento' => 'max:255',
            'Portada' => 'max:255',
            'Compania' => 'max:255',
            'Consolas' => 'max:255'
         ];
         
         $this->validate($request,$rules);
         $videojuego = Videogame::findOrFail($id);
         $videojuego->fill($request->all());
         if($videojuego->isClean()){
            return $this->errorResponse('Debe cambiar al menos un valor', Response::HTTP_UNPROCESSABLE_ENTITY);
         }
         if($request->hasfile('Portada')){
            $Portada = $request->file('Portada');
            $extension = $Portada->getClientOriginalExtension();
            $nombre = time() . '.' . $extension;
            $Portada->move('Portada/' , $nombre);
            
            $videojuego->Portada=$nombre;
        }
        $videojuego->save();
        return $this->successResponse($videojuego);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $videojuego = Videogame::findOrFail($id);
        $videojuego->delete();
        return $this->successResponse($videojuego);
    }
    //
}
