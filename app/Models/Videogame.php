<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Videogame extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'Nombre',
        'Descripcion',
        'Lanzamiento',
        'Portada',
        'Compania',
        'Consolas'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var string[]
     */
    
}
