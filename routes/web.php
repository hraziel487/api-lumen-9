<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//Controlador de Videojuegos
Route::get('/videojuegos', 'VideogameController@index');
Route::post('/videojuegos', 'VideogameController@store');
Route::get('/videojuegos/{id}','VideogameController@show');
Route::get('/videojuegos/{id}/edit','VideogameController@edit');
Route::put('/videojuegos/{id}','VideogameController@update');
Route::delete('/videojuegos/{id}','VideogameController@destroy');